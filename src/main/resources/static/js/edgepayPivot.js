/***********
name: Edgepay javascript plugin
version: 1.0.0
author: Edgepay 
**************//*
"use strict";
(function(window, document){
	
   var EdgePaySecureData = {};
   
   function EdgePayLib(){
	   this._isInt = function(n){
			if(Number(n)==parseInt(n))return true;
			return false;
	   };
	   this.length = function(n){
			return n.toString().length;
	   };
	   this._isEmpty = function(v){
		   if(v.trim()==""){return true;}
		   return false;
	   };
	   this._isCard = function(v){
            if (v.match(/^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12})|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/)){
                return true;
            }
            return false;
	   };
	   this._isExpDate = function(v){
			var d = v?v.match(/.{1,2}/g): [];
			var rm = (new Date()).getMonth()+1;
			var ry = parseInt((new Date()).getFullYear().toString().substr(-2));
			if(d.length && d.length==2 && this._isInt(d[0]) && this._isInt(d[1]) && parseInt(d[0])>0 && parseInt(d[0])<13){
				if(d[1]>ry || (d[1]==ry && d[0]>=rm)){
					return true;
				}
				return false;
			}
			return false;
	   };
       this.XMLHttpRequest = function(){
	        if (window.XMLHttpRequest) {
				return new XMLHttpRequest();
			} else {
				return new ActiveXObject("Microsoft.XMLHTTP");
			}
	   };
	   this.post = function(json, dis){
	        var http = this.XMLHttpRequest();
			http.onreadystatechange = function(){
			    if(http.readyState === 4 ){
				    if(http.status === 200){ 
					   if(json.success)json.success.call(dis, http.responseText, http.status);
					} else {
					   var stsData = http.responseText?JSON.parse(http.responseText):http.statusText;
					   if(json.error)json.error.call(dis, stsData, http.status);
					}
                }				
			}
			http.open('POST', json.url);
			if(json.headers && json.headers.length){
			   json.headers.forEach(function(head){
			      http.setRequestHeader(head.label, head.value);
			   });
			}
			http.send(JSON.stringify(json.data));
	   };
   }
   
   window.EdgePay = new function(){
       this.tokenSettings = null;
	   this.EVENT_SUCCESS = "success";
	   this.EVENT_ERROR = "error";
	   this.errorText = {
		   "required":"This is required",
		   "number":"Must be a number",
		   "expDateLength":"Must be as MMYY format",
		   "expDate":"Invalid Expiration Date",
		   "cvv2":"Length must be xxxx or xxx",
		   "cardNumber":"It is not a proper card number"
	   };
	   this.EVENT_INVALID = null;
	   this.lib = function(){
	       return new EdgePayLib();
	   };
	   this.getFormData = function(){
	       var data = {}, arr = ["cardNumber", "cardExpirationDate", "cvv2"];
		   for(var i=0; i<arr.length; i++){
		      data[arr[i]] = document.getElementById(this.tokenSettings[arr[i]])?document.getElementById(this.tokenSettings[arr[i]]).value:null;
		   }
		   return data;
	   };
	   this.getValidation = function(formData){
		   var cardNumber = formData.cardNumber;
		   var cardExpirationDate = formData.cardExpirationDate;
		   var cvv2 = formData.cvv2;
		   var error = [];
		   if(this.lib()._isEmpty(cardNumber) || !this.lib()._isInt(cardNumber) || !this.lib()._isCard(cardNumber)){
			   var errorText = "";
			   if(this.lib()._isEmpty(cardNumber)){errorText=this.errorText.required;}
			   else if(!this.lib()._isInt(cardNumber)){errorText=this.errorText.number;}
			   else if(!this.lib()._isCard(cardNumber)){errorText=this.errorText.cardNumber;}
			   if(errorText)error.push({fieldName: "cardNumber", errorText: errorText});
		   }
		   if(this.lib()._isEmpty(cardExpirationDate) || !this.lib()._isInt(cardExpirationDate) || !this.lib()._isExpDate(cardExpirationDate)){
			   var errorText = "";
			   if(this.lib()._isEmpty(cardExpirationDate)){errorText=this.errorText.required;}
			   else if(!this.lib()._isInt(cardExpirationDate)){errorText=this.errorText.number;}
			   else if(cardExpirationDate.toString().length!=4){errorText=this.errorText.expDateLength;}
			   else if(!this.lib()._isExpDate(cardExpirationDate)){errorText=this.errorText.expDate;}
			   if(errorText)error.push({fieldName: "cardExpirationDate", errorText: errorText});
		   }
		   if(!this.lib()._isEmpty(cvv2)){
			   var errorText = "";
			   if(!this.lib()._isInt(cvv2)){errorText=this.errorText.number;}
			   else if(cvv2.toString().length!=3 && cvv2.toString().length!=4){errorText=this.errorText.cvv2;}
			   if(errorText)error.push({fieldName: "cvv2", errorText: errorText});
		   }
		   if(error.length){
			   this.EVENT_INVALID = error;
			   return false;
		   }
		   this.EVENT_INVALID = null;
		   return true;
	   };
	   this.onload = function(){
	       var D = this;
	       window.onload = function(){
		     if(D.tokenSettings && D.tokenSettings.submitButton){
			    var submitButton = document.getElementById(D.tokenSettings.submitButton);
				if(submitButton){
                   submitButton.addEventListener("click", function(event){
                	  var formData = D.getFormData();
                	  var validate = D.getValidation(formData);
				      var formValidation = D.tokenSettings.isValidate?D.tokenSettings.isValidate():true;
					  if(validate && formValidation){
						  var extRefID = Math.floor(Math.random() * 899999999345345233);
						  var authKey = EdgePaySecureData.pivotKey;
						  var header = [{label: "Access-Control-Allow-Origin", value : "*"},
										{label: "authKey", value : authKey},
										{label: "externalReferenceID", value : extRefID}];
						  D.lib().post({url: EdgePaySecureData.serviceUrl, data: formData, headers: header, success: function(data){
							 if(typeof this.tokenSettings.eventListener=="function"){
								var token = null;
								try{
									var data = JSON.parse(data);
									if(data.tokenID){
										token = data.tokenID;
									}
								}catch(e){
									console.log("Something is wrong.");
								}
								this.tokenSettings.eventListener.call(this, {
								   edgepay_token: token,
								   response: data,
								   event: "success"
								});
							 }
						  }, error: function(data){
							 if(typeof this.tokenSettings.eventListener=="function"){
								this.tokenSettings.eventListener.call(this, {
								   edgepay_token: null,
								   response: data,
								   event: "error"
								});
							 }
						  }}, D);
					  }
				      event.stopImmediatePropagation();
				      event.preventDefault();
				   });
				}				
			 }
		  };
	   };
       this.init = function(json){
		  EdgePaySecureData.pivotKey = json.authToken?json.authToken:"";
		  EdgePaySecureData.serviceUrl = json.url?json.url:"";
		  this.onload();
	   };
	   this.token = function(tokenSettings){
	      this.tokenSettings = tokenSettings ? tokenSettings : null;
	   };
   }
      
}(window, document));*/


/***********
name: Edgepay javascript plugin
version: 1.0.0
author: Edgepay 
**************/
"use strict";
(function(window, document){
       
   var EdgePaySecureData = {};
   
   function EdgePayLib(){
          this._isInt = function(n){
                     if(Number(n)==parseInt(n))return true;
                     return false;
          };
          this.length = function(n){
                     return n.toString().length;
          };
          this._isEmpty = function(v){
                 if(v.trim()==""){return true;}
                 return false;
          };
          this._isCard = function(v){
            if (v.match(/^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12})|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/)){
                return true;
            }
            return false;
          };
          this._isExpDate = function(v){
                     var d = v?v.match(/.{1,2}/g): [];
                     var rm = (new Date()).getMonth()+1;
                     var ry = parseInt((new Date()).getFullYear().toString().substr(-2));
                     if(d.length && d.length==2 && this._isInt(d[0]) && this._isInt(d[1]) && parseInt(d[0])>0 && parseInt(d[0])<13){
                           if(d[1]>ry || (d[1]==ry && d[0]>=rm)){
                                  return true;
                           }
                           return false;
                     }
                     return false;
          };
       this.XMLHttpRequest = function(){
               if (window.XMLHttpRequest) {
                           return new XMLHttpRequest();
                     } else {
                           return new ActiveXObject("Microsoft.XMLHTTP");
                     }
          };
          this.post = function(json, dis){
               var http = this.XMLHttpRequest();
                     http.onreadystatechange = function(){
                         if(http.readyState === 4 ){
                               if(http.status === 200){ 
                                     if(json.success)json.success.call(dis, http.responseText, http.status);
                                  } else {
                                     var stsData = http.responseText?JSON.parse(http.responseText):http.statusText;
                                     if(json.error)json.error.call(dis, stsData, http.status);
                                  }
                }                        
                     }
                     http.open('POST', json.url);
                     if(json.headers && json.headers.length){
                        json.headers.forEach(function(head){
                           http.setRequestHeader(head.label, head.value);
                        });
                     }
                     http.send(JSON.stringify(json.data));
          };
   }
   
   window.EdgePay = new function(){
       this.tokenSettings = null;
          this.EVENT_SUCCESS = "success";
          this.EVENT_ERROR = "error";
          this.errorText = {
                 "required":"This is required",
                 "number":"Must be a number",
                 "expDateLength":"Must be as MMYY format",
                 "expDate":"Invalid Expiration Date",
                 "cvv2":"Length must be xxxx or xxx",
                 "cardNumber":"It is not a proper card number"
          };
          this.EVENT_INVALID = null;
          this.lib = function(){
              return new EdgePayLib();
          };
          this.getFormData = function(){
              var data = {}, arr = ["cardNumber", "cardExpirationDate", "cvv2"];
                 for(var i=0; i<arr.length; i++){
                    data[arr[i]] = document.getElementById(this.tokenSettings[arr[i]])?document.getElementById(this.tokenSettings[arr[i]]).value:null;
                 }
                 return data;
          };
          this.getValidation = function(formData){
                 var cardNumber = formData.cardNumber;
                 var cardExpirationDate = formData.cardExpirationDate;
                 var cvv2 = formData.cvv2;
                 var error = [];
                 if(this.lib()._isEmpty(cardNumber) || !this.lib()._isInt(cardNumber) || !this.lib()._isCard(cardNumber)){
                        var errorText = "";
                        if(this.lib()._isEmpty(cardNumber)){errorText=this.errorText.required;}
                        else if(!this.lib()._isInt(cardNumber)){errorText=this.errorText.number;}
                        else if(!this.lib()._isCard(cardNumber)){errorText=this.errorText.cardNumber;}
                        if(errorText)error.push({fieldName: "cardNumber", errorText: errorText});
                 }
                 if(this.lib()._isEmpty(cardExpirationDate) || !this.lib()._isInt(cardExpirationDate) || !this.lib()._isExpDate(cardExpirationDate)){
                        var errorText = "";
                        if(this.lib()._isEmpty(cardExpirationDate)){errorText=this.errorText.required;}
                        else if(!this.lib()._isInt(cardExpirationDate)){errorText=this.errorText.number;}
                        else if(cardExpirationDate.toString().length!=4){errorText=this.errorText.expDateLength;}
                        else if(!this.lib()._isExpDate(cardExpirationDate)){errorText=this.errorText.expDate;}
                        if(errorText)error.push({fieldName: "cardExpirationDate", errorText: errorText});
                 }
                 if(!this.lib()._isEmpty(cvv2)){
                        var errorText = "";
                        if(!this.lib()._isInt(cvv2)){errorText=this.errorText.number;}
                        else if(cvv2.toString().length!=3 && cvv2.toString().length!=4){errorText=this.errorText.cvv2;}
                        if(errorText)error.push({fieldName: "cvv2", errorText: errorText});
                 }
                 if(error.length){
                        this.EVENT_INVALID = error;
                        return false;
                 }
                 this.EVENT_INVALID = null;
                 return true;
          };
          this.onload = function(){
              var D = this;
              window.onload = function(){
                   if(D.tokenSettings && D.tokenSettings.submitButton){
                         var submitButton = document.getElementById(D.tokenSettings.submitButton);
                           if(submitButton){
                   submitButton.addEventListener("click", function(event){
                       var formData = D.getFormData();
                       var validate = D.getValidation(formData);
                                 var formValidation = D.tokenSettings.isValidate?D.tokenSettings.isValidate():true;
                                    if(validate && formValidation){
                                           var extRefID = Math.floor(Math.random() * 899999999345345233);
                                           var authKey = EdgePaySecureData.pivotKey;
                                           var header = [{label: "Access-Control-Allow-Origin", value : "*"},
                                                                     {label: "authKey", value : authKey},
                                                                     {label: "externalReferenceID", value : extRefID}];
                                           D.lib().post({url: EdgePaySecureData.serviceUrl, data: formData, headers: header, success: function(data){
                                                if(typeof this.tokenSettings.eventListener=="function"){
                                                       var token = null;
                                                       try{
                                                              var data = JSON.parse(data);
                                                              if(data.tokenID){
                                                                     token = data.tokenID;
                                                              }
                                                       }catch(e){
                                                              console.log("Something is wrong.");
                                                       }
                                                       this.tokenSettings.eventListener.call(this, {
                                                          edgepay_token: token,
                                                          response: data,
                                                          event: "success"
                                                       });
                                                }
                                           }, error: function(data){
                                                if(typeof this.tokenSettings.eventListener=="function"){
                                                       this.tokenSettings.eventListener.call(this, {
                                                          edgepay_token: null,
                                                          response: data,
                                                          event: "error"
                                                       });
                                                }
                                           }}, D);
                                    }
                                 event.stopImmediatePropagation();
                                 event.preventDefault();
                              });
                           }                          
                     }
                };
          };
       this.init = function(json){
                EdgePaySecureData.pivotKey = json.authToken?json.authToken:"";
                EdgePaySecureData.serviceUrl = json.url?json.url:"";
                this.onload();
          };
          this.token = function(tokenSettings){
             this.tokenSettings = tokenSettings ? tokenSettings : null;
          };
   }
      
}(window, document));


