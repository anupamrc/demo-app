package com.get.edgepay.mgmt.config;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;


@SpringBootApplication
@EnableAutoConfiguration
public class DemoServiceApplication {
	
    
    public static void main(String[] args) {
        SpringApplication.run(DemoServiceApplication.class, args);
    }
    
    /*@Bean
    public RestTemplate restTemplate() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException, UnrecoverableKeyException {
    	
    	SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(new SSLContextBuilder().loadTrustMaterial(null, new TrustSelfSignedStrategy()).build());
        HttpClient httpClient = HttpClients.custom().setSSLSocketFactory(socketFactory).build();

        RestTemplate template = new RestTemplate(new HttpComponentsClientHttpRequestFactory(httpClient));
        return template;
    }*/
    
    /*@Bean
    public RestTemplate restTemplate() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

        SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
                        .loadTrustMaterial(null, acceptingTrustStrategy)
                        .build();

        SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);

        CloseableHttpClient httpClient = HttpClients.custom()
                        .setSSLSocketFactory(csf)
                        .build();

        HttpComponentsClientHttpRequestFactory requestFactory =
                        new HttpComponentsClientHttpRequestFactory();

        requestFactory.setHttpClient(httpClient);
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        return restTemplate;
     }*/
    
    
    @Bean
    public RestTemplate restTemplate() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
    	SSLContext sslContext = new SSLContextBuilder()
    		      .loadTrustMaterial(null, (certificate, authType) -> true).build();
    		    CloseableHttpClient httpClient = HttpClients.custom()
    		      .setSSLContext(sslContext)
    		      .setSSLHostnameVerifier(new NoopHostnameVerifier())
    		      .build();
     
        HttpComponentsClientHttpRequestFactory requestFactory =
                        new HttpComponentsClientHttpRequestFactory();

        requestFactory.setHttpClient(httpClient);
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        return restTemplate;
     }
    
    /*@Bean
    public RestTemplate restTemplate() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
    	SSLContext sslContext = SSLContext.getInstance("SSL");

    	// set up a TrustManager that trusts everything
    	sslContext.init(null, new TrustManager[] { new X509TrustManager() {
    	            public X509Certificate[] getAcceptedIssuers() {
    	                    System.out.println("getAcceptedIssuers =============");
    	                    return null;
    	            }

    	            public void checkClientTrusted(X509Certificate[] certs,
    	                            String authType) {
    	                    System.out.println("checkClientTrusted =============");
    	            }

    	            public void checkServerTrusted(X509Certificate[] certs,
    	                            String authType) {
    	                    System.out.println("checkServerTrusted =============");
    	            }
    	} }, new SecureRandom());

    	SSLSocketFactory sf = new SSLSocketFactory(sslContext);
    	Scheme httpsScheme = new Scheme("https", 443, sf);
    	SchemeRegistry schemeRegistry = new SchemeRegistry();
    	schemeRegistry.register(httpsScheme);

    	// apache HttpClient version >4.2 should use BasicClientConnectionManager
    	ClientConnectionManager cm = new BasicClientConnectionManager(schemeRegistry);
    	HttpClient httpClient = new DefaultHttpClient(cm);     
        HttpComponentsClientHttpRequestFactory requestFactory =
                        new HttpComponentsClientHttpRequestFactory();

        requestFactory.setHttpClient(httpClient);
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        return restTemplate;
     }*/
    
}



/*@RestController
class MessageRestController {

	@Autowired
	LogListeners service;

	@RequestMapping(value = "/edgepay", method = RequestMethod.GET)
    String getMessage() {
		return service.testJDBCConnectionDetails();
    }
    
}*/