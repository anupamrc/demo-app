package com.get.edgepay.mgmt.config;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Map;

import javax.validation.Valid;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import lombok.Data;

@Controller
public class DemoController {

	@Value("${merchant-demo.edgepay_url}")
	private String EDGEPAY_URL;

	@Value("${merchant-demo.merchantID}")
	private String MERCHANT_ID;

	@Value("${merchant-demo.merchantKey}")
	private String MERCHANT_KEY;

	@Value("${merchant-demo.terminalID}")
	private String TERMINAL_ID;

	@Autowired
	RestTemplate restTemplate;

	private static final String SYSTEM_TIMESTAMP_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@RequestMapping(value = "/doPayment", method = RequestMethod.POST)
	public String submit(@Valid @ModelAttribute("paymentDto") PaymentDto paymentDto, BindingResult result,
			ModelMap model) {
		if (result.hasErrors()) {
			return "error";
		}
		logger.info("Payment Request To Merchant Server with Values...");
		logger.info("token=" + paymentDto.getToken());
		logger.info("amount=" + paymentDto.getAmount());
		logger.info("customerName=" + paymentDto.getCustomerName());
		logger.info("Tip=" + paymentDto.getTip());
		logger.info("BillingAdress=" + paymentDto.getBillingAdress());

		// CAlling Edgepay Payment URL
		SimpleDateFormat sdf = new SimpleDateFormat(SYSTEM_TIMESTAMP_FORMAT);
		sdf.setLenient(false);

		// String date = sdf.format(new Date());
		// String date = "2018-04-02T3:56:19";
		String date = dateformat();
		logger.info("Request Date::" + date);

		String serviceUrl = EDGEPAY_URL + "/payment";
		String exterRefId = "222222" + String.valueOf(System.currentTimeMillis());
		JSONObject jsonObj = new JSONObject();
		try {
			jsonObj.put("merchantID", MERCHANT_ID);
			jsonObj.put("terminalID", TERMINAL_ID);
			jsonObj.put("captureToo", "Yes");
			jsonObj.put("paymentDataInput", "Manualentry");
			jsonObj.put("amount", paymentDto.getAmount());
			jsonObj.put("tokenID", paymentDto.getToken());
			jsonObj.put("tip", paymentDto.getTip());
			jsonObj.put("billingAddress", paymentDto.getBillingAdress());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String request = jsonObj.toString();
		logger.info("Request String::" + request);

		ResponseEntity<PaymentResponse> serviceResponse = null;

		HttpHeaders headers = new HttpHeaders();
		// Add header params
		headers.set("Content-Type", "application/json");
		headers.set("externalReferenceID", exterRefId);
		headers.set("merchantKey", MERCHANT_KEY);
		headers.set("transactionDate", date);
		System.out.println("Header done..");
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(serviceUrl);

		try {
			HttpEntity<?> httpRequest = new HttpEntity<>(request, headers);
			// restTemplate.
			serviceResponse = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.POST, httpRequest,
					PaymentResponse.class);

			if (serviceResponse.getStatusCode() == HttpStatus.OK) {
				PaymentResponse response = (PaymentResponse) serviceResponse.getBody();
				model.put("message", "Payment Processed successfully. Transaction Id:" + response.getTransactionID());
				System.out.println("Success Response::::: "+response.getTransactionID());
			} 
		}  catch (HttpStatusCodeException e) {
			String errorpayload = e.getResponseBodyAsString();
			System.out.println("Error Response::::: "+errorpayload);
			model.put("message", "Payment Declined");
		} catch (RestClientException e) {
			System.out.println("Error Response::::: "+e.getMessage());
			e.printStackTrace();
			model.put("message", "Payment Declined");
		}

		model.put("display", "none");
		return "merchantDemo";
	}

	@RequestMapping("/demoPayment")
	public String merchantSideCode(Map<String, Object> model) {
		JSONObject jsonObj = new JSONObject();
		try {
			jsonObj.put("merchantID", MERCHANT_ID);
			jsonObj.put("terminalID", TERMINAL_ID);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String exterRefId = "222222" + String.valueOf(System.currentTimeMillis());
		String serviceUrl = EDGEPAY_URL + "/generatePivoteAuthKey";

		// String
		// request="{\"merchantID\":\"7991234560716182757\",\"terminalID\":\"8666888899990000000\"}";
		String request = jsonObj.toString();
		logger.info("Request String::" + request);
		String authToken = null;

		// RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<MerchantAuthResponse> serviceResponse = null;

		HttpHeaders headers = new HttpHeaders();
		// Add header params

		headers.set("externalReferenceID", exterRefId);
		headers.set("merchantKey", MERCHANT_KEY);
		System.out.println("Header done..");
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(serviceUrl);

		HttpEntity<?> httpRequest = new HttpEntity<>(request, headers);

		try {
			serviceResponse = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.POST, httpRequest,
					MerchantAuthResponse.class);
		} catch (HttpStatusCodeException e) {
			String errorpayload = e.getResponseBodyAsString();
			System.out.println("Error Response::::: "+errorpayload);
		} catch (RestClientException e) {
			System.out.println("Error Response::::: "+e.getMessage());
		}

		if (serviceResponse.getStatusCode() == HttpStatus.OK) {
			MerchantAuthResponse authResponse = (MerchantAuthResponse) serviceResponse.getBody();

			authToken = authResponse.getAuthToken();

		} else {
			// TODO How Merchant want to handle the error scenario
		}

		model.put("authToken", authToken);
		model.put("tokenURL", EDGEPAY_URL + "/tokenSU");
		model.put("display", "block");
		return "merchantDemo";
	}

	public static String dateformat() {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			GregorianCalendar gc = new GregorianCalendar();
			// String dateString = sdf.format(gc.getTime());
			String dateString = sdf.format(System.currentTimeMillis());
			System.out.println(dateString);
			return dateString;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		// gc.setTime(sdf.parse(dateString));
		// XMLGregorianCalendar date2 =
		// DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
	}

}

@Data
class MerchantAuthResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	private String result;
	private String responseCode;
	private String responseMessage;
	private String authToken;
}

@Data
class PaymentDto implements Serializable {

	private static final long serialVersionUID = 1L;
	String amount;
	String customerName;
	String token;
	String tip;
	String billingAdress;
}

@Data
class PaymentResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private String result;
	private String responseCode;
	private String responseMessage;
	private String merchantID;
	private String terminalID;
	private String timestamp;
	private String securityResult; // Need to discuss
	private String authCode;
	private String processor;
	private String amount;
	private String currencyCode;
	private String maskedAccount;
	private String cardExpirationDate;
	private String tokenID;
	private String customerNumber;
	private String transactionID;
	private String avsResponse;
	private String cvvResponse;
	private String commercialCard;
	private String emvArpc;
	private String recurringResult;
	private String avsCode;
	private String cvvCode;
	private String merchantField1;
	private String merchantField2;

}