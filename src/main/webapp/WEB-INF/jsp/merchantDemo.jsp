<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>
<link rel="stylesheet" type="text/css" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css" />
<!-- <script src="https://qaenv-edgepay-web-gateway2.us-west-1.elasticbeanstalk.com/js/edgepayPivot.min.js"></script> -->

<script type="text/javascript" src="js/edgepayPivot.js"></script>


<style>
body {
	font-family: "Avenir Next", "Nunito", Arial, sans-serif;
	color: #444444;
	background-color: #04233d;
}

h1 {
	font-size: 26px;
	background: #eee;
	margin: 0;
	padding: 10px;
}

h2 {
	font-size: 20px;
}

.container {
	background: #fff;
	width: 600px;
}
</style>
</head>
<body>
	<div class="container">
		<div class="starter-template">
			<h1>Merchant Demo Payment Page</h1>
		</div>
		<h2>Sale Request::</h2>
		<h4>${message}</h4>
		<div style="display :${display}">
			<form method="POST" action="/doPayment" id="paymentForm"
				name="paymentForm">
				<table class="table">
					<tr class="form-group">
						<td width="30%">Sale</td>
						<td></td>
					</tr>
					<tr class="form-group">
						<td width="30%">Currency</td>
						<td>USD</td>
					</tr>
					<tr class="form-group">
						<td width="30%">Card Number</td>
						<td><input class="form-control" type="text" id="cardNumber"
							name="cardNumber" /></td>
					</tr>
					<tr class="form-group">
						<td width="30%">Card Expiration Date</td>
						<td><input class="form-control" type="text" id="expiryDate"
							name="expiryDate" /></td>
					</tr>
					<tr class="form-group">
						<td width="30%">CVV2</td>
						<td><input class="form-control" type="text" id="cvv2"
							name="cvv2" /></td>
					</tr>
					<tr class="form-group">
						<td width="30%">Customer Name</td>
						<td><input class="form-control" type="text" id="customerName"
							value="ROY" name="customerName" /></td>
					</tr>
					<tr class="form-group">
						<td width="30%">Amount</td>
						<td><input class="form-control" type="text" id="amount"
							name="amount" /></td>
					</tr>
					<tr class="form-group">
						<td width="30%">Tip</td>
						<td><input class="form-control" type="text" id="tip"
							name="tip" /></td>
					</tr>
					<tr class="form-group">
						<td width="30%">Billing Address</td>
						<td><input class="form-control" type="text"
							id="billingAdress" name="billingAdress" /></td>
					</tr>
					<tr class="form-group">
						<td width="30%">Auth Key</td>
						<td><textarea class="form-control"  id="jwt"
							name="jwt" >${authToken}</textarea></td>
					</tr>
					<tr class="form-group">
						<td width="30%"><input class="form-control" type="hidden"
							id="token" name="token" /></td>
						<td><input type="button" class="btn btn-primary"
							value="Submit" id="payButton" /></td>
					</tr>
				</table>
			</form>
		</div>
	</div>

	<script>
	
	   EdgePay.init({
		   url: "${tokenURL}",
		   authToken: "${authToken}"
	   });
	   
	   EdgePay.token({
	        submitButton: "payButton",     
	        cardNumber: "cardNumber",     
	        cardExpirationDate: "expiryDate",
			cvv2: "cvv2",
			isValidate: function(){
				if(EdgePay.EVENT_INVALID)alert(JSON.stringify(EdgePay.EVENT_INVALID))
			    return true;
			},
	        eventListener: function(eventData){
			    console.log(eventData);
	            if (eventData.event == EdgePay.EVENT_SUCCESS) {
					alert(JSON.stringify(eventData));
					document.getElementById("token").value = eventData.edgepay_token;
					document.getElementById("paymentForm").submit();
					
					//Need to aquire the token from the response 
					//New Single use Token can be set to a form field before form submission/ajax call to the merchant's server 
	    		}else if (eventData.event == EdgePay.EVENT_ERROR){
	    			alert(JSON.stringify(eventData));
	    			//Do required as per business need
		        }
	        }    
	   });
	    
	</script>
	
	
	